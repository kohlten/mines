<h1>Mines</h1>
Mines is a simple minesweeper remake.

Compiling:
```
   git clone https://gitlab.com/kohlten/mines
   cd mines
   git clone https://github.com/SFML/SFML.git
   cd SFML
   mkdir build
   cd build
   cmake ..
   make && sudo make install
   cd ../../
   git clone https://github.com/SFML/CSFML.git
   cd CSFML
   mkdir build
   cd build
   cmake ..
   make && sudo make install
   cd ../../
   git clone https://github.com/Jebbs/DSFMLC.git
   cd DSFMLC
   mkdir build
   cd build
   cmake ..
   make && sudo make install
   cd ../../
   dub build
   ./bin/mines
```

![one](/images/1.png)
![two](/images/2.png)
![three](/images/3.png)
![fourd](/images/4.png)

